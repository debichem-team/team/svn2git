#!/bin/bash

set -ex

path=$(dirname $(readlink -f $0))
source ${path}/svn2git.rc

cd $REPDIR
rm -rf tmp/
mkdir tmp/
cd tmp/

{
svn-all-fast-export --identity-map=${REPDIR}/authors.txt --rules=${REPDIR}/debichem.rules --stats ${SVNREP}

repos=$(find $(pwd) -mindepth 1 -maxdepth 1 -type d ! -name "*.*" | sort)
echo $repos

for r in $repos
do
	${REPDIR}/gitfixtags.sh $r
	fixed="${r/%/.fixed}"
	bare="${r/%/.git}"
	proj="`basename $r`"
	mv $r $fixed

	echo "Create ${bare}"
	git clone --bare $fixed $bare
	cp $fixed/description $bare/description

	# todo: check, that master exists!
	if [ -z "`git -C $bare --bare show-ref | grep 'refs/heads/master'`" ] &&
	   [    "`git -C $bare --bare symbolic-ref HEAD`" = 'refs/heads/master' ] &&
	   [ -n "`git -C $bare --bare show-ref | grep 'refs/heads/wnpp'`" ]
	then
		git -C $bare --bare symbolic-ref -m "Setting head to wnpp without master." HEAD refs/heads/wnpp
		# todo: or make wnpp the new master branch?
	fi

	# now clone and fix Vcs-Tags in HEAD
	if [ ${FIX_VCSFIELDS} ] && [ -z "`echo ${EXCLUDE_REPOS} | grep -w ${proj}`" ] && [ -n "$FIX_VCSFIELDS_AUTHOR" ]
	then
		echo "Fix Vcs fields if possible in ${bare}"
		git clone -l $bare
		if [ -f ${r}/debian/control ]
		then
			sed -i \
			    -e "s#\(Vcs-Browser:\) .*\$#\1 https://salsa.debian.org/debichem-team/${proj}#g" \
			    -e "s#Vcs-Svn: .*\$#Vcs-Git: https://salsa.debian.org/debichem-team/${proj/%/.git}#g" \
			    -e "s#Vcs-Git: .*\$#Vcs-Git: https://salsa.debian.org/debichem-team/${proj/%/.git}#g" \
			    ${r}/debian/control
			[ -n "`git -C $r diff`" ] && git -C $r commit \
			    --author="${FIX_VCSTAGS_AUTHOR}" \
			    -m "Fix Vcs-* fields in debian/control after migrating package to Git." \
			    debian/control
		fi
		git -C $r push || true
	fi
	echo "Repacking ${bare}"
	git -C $bare repack -a -d -f
done

if [ ${SALSA_DELETE_BEFORE_UPLOAD} ]
then
	for r in $repos
	do
		proj="`basename $r`"
		[ -n "`echo ${EXCLUDE_REPOS} | grep -w ${proj}`" ] && continue
		${REPDIR}/gitimport.sh $r del
	done
fi

if [ ${SALSA_IMPORT_REPOS} ] && [ -n "$UPLOAD_URL" ]
then
	echo "Uploading the repositories to ${UPLOAD_URL}"
	rsync -avz --delete *.git "$UPLOAD_URL"
	for r in $repos
	do
		proj="`basename $r`"
		bare="${r/%/.git}"
		[ -n "`echo ${EXCLUDE_REPOS} | grep -w ${proj}`" ] && continue
		echo "Import repository"
		${REPDIR}/gitimport.sh ${bare} import
	done
else
	for r in $repos
	do
		proj="`basename $r`"
		bare="${r/%/.git}"
		[ -n "`echo ${EXCLUDE_REPOS} | grep -w ${proj}`" ] && continue
		echo "Creating remote repository"
		${REPDIR}/gitimport.sh $r add
		echo "Setting remote origin for ${proj}"
		git -C $bare remote set-url origin ${SALSA_ORIGIN}/${proj/%/.git}
		echo "Pushing ${proj/%/.git} to ${SALSA_ORIGIN}/${proj/%/.git}"
		git -C $bare push --all origin
		git -C $bare push --tags origin
	done
fi

} 2>&1 > >(tee -a -i svn2git.log)

cd $REPDIR

exit 0
