These files were used to migrate the debichem subversion repository (~100MB,
\>10 years of usage) into separate (per package) Git repositories. The scripts
used also fixed all glitches like mixed-revision-tags and set the Vcs\* fields
in debian/control automatically to the new destination.

The project files are no longer developed as they've fulfilled their purpose.
But they may demonstrate the usage of `svn-all-fast-export` and help others.

All files including the scripts are released into public domain. See
[LICENSE](/LICENSE).
