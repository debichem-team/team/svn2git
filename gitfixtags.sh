#!/bin/bash

set -e

rep=$1
echo "Backing up $rep..."
cp -a "$rep" "${rep%/}.orig"

echo "Start controlling/fixing the tags in $rep..."
tag_list=`git -C $rep --bare tag -l 'debian/*'`
for t in $tag_list
do
	cid=""
	parents=`git -C $rep --bare log --pretty=%P -1 $t`
	for p in $parents
	do
		#echo "Comparing $p with $t."
		diff=`git -C $rep --bare diff-tree -r -p --raw $p $t`
		if [ -z "$diff" ]
		then
			if [ -z "$cid" ]
			then
				cid=$p
				echo "Found tree-matching parent ID $p"
			else
				echo "This shouldn't happen."
				exit 1
			fi
		fi
	done
	if [ -n "$cid" ]
	then
		echo "Retagging $t with commit ID $cid."
		subject=`git -C $rep --bare log -1 --pretty=format:"%s" $t`
		GIT_COMMITTER_DATE=`git -C $rep --bare log -1 --pretty=format:"%ci" $t` \
		    git -C $rep --bare tag -f -m "$subject" "$t" "$cid"
	fi
done
exit 0
