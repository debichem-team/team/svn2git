#!/bin/bash

set -ex

path=$(dirname $(readlink -f $0))
source ${path}/svn2git.rc

if [ -z $@ ]
then
	echo "Without any argument, we will work on the REPOS specified in svn2git.rc."
	excludes=${EXCLUDE_REPOS}
else
	excludes=
fi

projects=${@:-${REPOS}}

for p in $projects
do
	[ -z "`echo ${excludes} | grep -w ${p}`" ] || continue
	echo "Enabling \"Emails on push\" service for project ${SALSA_GROUP}/${p} ..."
	curl --request PUT \
	     --header "PRIVATE-TOKEN: ${SALSA_TOKEN}" \
	     --data "send_from_committer_email=1&disable_diffs=0&recipients=${SALSA_PROJECT_PUSH_MAIL}" \
	     ${SALSA_URL}/projects/${SALSA_GROUP}%2F${p}/services/emails-on-push
done

exit 0
