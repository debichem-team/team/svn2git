#!/bin/bash

set -eux

path=$(dirname $(readlink -f $0))
source ${path}/svn2git.rc

function delete_rep {
	curl --request DELETE --header "PRIVATE-TOKEN: $SALSA_TOKEN" \
	    https://salsa.debian.org/api/v4/projects/${SALSA_GROUP}%2F${PROJECT}
}

function create_rep {
	curl --request POST --header "PRIVATE-TOKEN: $SALSA_TOKEN" \
	     --data "path=$PROJECT&namespace_id=$SALSA_NAMESPACE&description=$DESCRIPTION&visibility=public" \
	     ${SALSA_URL}/projects
}

function import_rep {
	[ -n "$IMPORT_URL" ] || exit 1
	curl --request POST --header "PRIVATE-TOKEN: $SALSA_TOKEN" \
	     --data "path=$PROJECT&namespace_id=$SALSA_NAMESPACE&description=$DESCRIPTION&import_url=$IMPORT_URL/${PROJECTDIR}&visibility=public" \
	     ${SALSA_URL}/projects
}

PROJECTDIR="`basename ${1%/}`"
PROJECT="${PROJECTDIR%.git}"
DESCRIPTION="`cat ${1}/description`" || DESCRIPTION="`cat ${1/%/.git}/description`" || \
    DESCRIPTION="`cat ${1}/.git/description`" || DESCRIPTION="${PROJECT} packaging"

if [ $# -ge 2 ]
then
	case "$2" in
	"add")
		create_rep
	;;
	"del")
		delete_rep
	;;
	"import")
		import_rep
	;;
	*)
		#delete_rep # mainly for testing
		create_rep
	;;
	esac
else
	#delete_rep # mainly for testing
	create_rep
fi

exit 0
